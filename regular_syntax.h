#ifndef REGULAR_SYNTAX_H_
#define REGULAR_SYNTAX_H_

namespace regular_syntax {

template<class T>
using pointer = T*;

template<class T>
using const_ = const T;

template<class T>
using volatile_ = volatile T;

template<class T>
using lref = T&;

template<class T>
using rref = T&&;

template<class T>
using carray_nosize = T[];

template<class T, size_t Sz>
using carray = T[Sz];

template<class R, class ...Args>
using cfunction = R(Args...);

template<class R, class ...Args>
using cfunction_va = R(Args..., ...);

template<class C, class R>
using pfield = R (C::*);

template<class C, class R, class ...Args>
using pmethod = R (C::*)(Args...);

template<class C, class R, class ...Args>
using pmethod_va = R (C::*)(Args..., ...);

template<class C, class R, class ...Args>
using pmethod_const = R (C::*)(Args...) const;

template<class C, class R, class ...Args>
using pmethod_va_const = R (C::*)(Args..., ...) const;

} /* regular_syntax */

#endif /* REGULAR_SYNTAX_H_ */
