One of the things I really hate about C++ is the C syntax for pointers/references to functions/arrays/methods and the like.

How do you declare an alias type that is a pointer to function?
`typedef void (*name)(int);`!
Or is it `typedef void *name(int)`?
Or `typedef void (name*)(int)`?

Everytime I try to deal with this types I get confused.
So fuck you, C syntax. We have templates in C++.

This is what you do when you need a const pointer to function:
```
const_<pointer<cfunction<void, int>>> pfunc = foo;
```
And this is what you do when you need a reference to array:
```
lref<carray<const_<char>>, 12>> arref = "Hello world";
```

Long and unnecessarily complex? Yes, it is. But it is _consistent_. And you would never make a mistake typing it in.
